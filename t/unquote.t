# -*- perl -*-

use Test::More tests => 16;
use Test::Exception;

use Ucam::Starling::Git;

# In Perl, even single quotes convert '\\' to \.
is(length('\\\\'), 2);

my @uqtest = (
   [ 'foo' => "foo" ],
   [ '"foo"' => "foo" ],
   [ '"foo" ' => "foo" ],
   [ '"foo "' => "foo " ],
   [ '"\\"foo"' => "\"foo" ],
   [ '"\\\\\\\\""' => "\\\\" ],
   [ '"\\\\\\\\\\""' => "\\\\\"" ],
   [ '"\\a"' => "\x07" ],
   [ '"\\b"' => "\x08" ],
   [ '"\\t"' => "\x09" ],
   [ '"\\n"' => "\x0a" ],
   [ '"\\f"' => "\x0c" ],
   [ '"\\r"' => "\x0d" ],
   [ '"\\000"' => "\x00" ],
   [ '"\\040"' => " " ],
);

for (@uqtest) {
    is(Ucam::Starling::Git::unquote_c_style($_->[0]),  $_->[1]);
}
