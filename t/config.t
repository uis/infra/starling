# -*- perl -*-

use Test::More tests => 11;
use Test::Exception;

use Ucam::Starling;

use File::Temp;

my $dir = File::Temp->newdir;

$ENV{PREFIX} = $dir;
$ENV{HOME} = $dir;

dies_ok { Ucam::Starling::config; } 'failure if file not found';

mkdir "$dir/etc" or die;
mkdir "$dir/etc/starling" or die;

sub set_file {
    if (defined $_[1]) {
	open my $f, "> $_[0]" or die;
	print $f $_[1];
	close $f or die;
    } else {
	unlink $_[0];
    }
}

sub set_sysconf {
    set_file("$dir/etc/starling/starling.conf", @_);
}

set_sysconf "foo\n";
dies_ok { Ucam::Starling::sysconfig; } 'failure if not a mapping';

my $cfg;
set_sysconf "foo: bar\n";
lives_ok { $cfg = Ucam::Starling::sysconfig; } 'success if a mapping';
is_deeply($cfg, { foo => 'bar' }, 'correct decoding');

sub set_userconf {
    set_file("$dir/etc/starling/starling.conf", undef);
    set_file("$dir/etc/starling/user.conf", $_[0]);
    set_file("$dir/.starling", $_[1]);
}

set_userconf(undef, undef, undef);
lives_ok { $cfg = Ucam::Starling::userconfig; } 'user conf need not exist';
is_deeply($cfg, { }, "... but is empty if it doesn't");

set_userconf("foo: bar\n", undef);
lives_ok { $cfg = Ucam::Starling::userconfig; } 'load system-wide userconf';
is_deeply($cfg, { foo => 'bar' }, '... and decode it');

set_userconf(undef, "foo: bar\n");
lives_ok { $cfg = Ucam::Starling::userconfig; } 'load userconf';
is_deeply($cfg, { foo => 'bar' }, '... and decode it');

set_userconf("x: a\ny: b\n", "y: c\nz: d\n");
is_deeply(Ucam::Starling::userconfig,
	  { x => 'a', y => 'c', z => 'd' }, 'merging userconf');
