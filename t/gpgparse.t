# -*- perl -*-

use Test::More tests => 1;
use Test::Exception;

use Ucam::Starling;

# Tests for parsing of GnuPG key lists.

sub parses_as ($$) {
    my @lines = split(/^/, $_[0]);
    is_deeply([Ucam::Starling::gpg_parse_keylist(@lines)], $_[1]);
}

parses_as(<<'EOF'
tru::0:1328114777:1338048636:3:1:5
pub:-:2048:1:E325E4E49190393C:1321625216:::-:::scESC:
fpr:::::::::10907C184B89B92BD353F792E325E4E49190393C:
uid:-::::1326391742::77F6AB4457AEB9BC1C95846B823F48E4550A289F::tag\x3awraith.csi.cam.ac.uk,2012-01-10\x3astarling\x3abranch\x3atestbox:
rev:::1:30B169145FAEAAC0:1328104065::::Ben Harris <bjh21@cam.ac.uk>:30x:
sig:::1:E325E4E49190393C:1326391742::::tag\x3awraith.csi.cam.ac.uk,2012-01-10\x3astarling\x3abranch\x3atestbox:13x:
sig:::1:30B169145FAEAAC0:1326392342::::Ben Harris <bjh21@cam.ac.uk>:10x:
uid:-::::1321625216::99E0C029A367A7D158C039350B2A0771D24AA7AE::testbox.csi.cam.ac.uk:
rev:::1:30B169145FAEAAC0:1328104067::::Ben Harris <bjh21@cam.ac.uk>:30x:
sig:::1:E325E4E49190393C:1321625216::::tag\x3awraith.csi.cam.ac.uk,2012-01-10\x3astarling\x3abranch\x3atestbox:13x:
sig:::1:30B169145FAEAAC0:1326392350::::Ben Harris <bjh21@cam.ac.uk>:10x:
sub:-:2048:1:243B41440626B2CD:1321625216::::::e:
sig:::1:E325E4E49190393C:1321625216::::tag\x3awraith.csi.cam.ac.uk,2012-01-10\x3astarling\x3abranch\x3atestbox:18x:
EOF
,
[ { _ => [ 'pub', '-', '2048', '1', 'E325E4E49190393C', '1321625216',
	   '', '', '-', '', '', 'scESC', '' ],
    fpr => [ 'fpr', '', '', '', '', '', '', '', '',
	     '10907C184B89B92BD353F792E325E4E49190393C', '' ],
    uid => [
	{ _ => [ 'uid', '-', '', '', '', '1326391742', '',
		 '77F6AB4457AEB9BC1C95846B823F48E4550A289F', '',
		 'tag:wraith.csi.cam.ac.uk,2012-01-10:starling:branch:testbox',
		 '' ],
	  sig => [
	      [ 'rev', '', '', '1', '30B169145FAEAAC0', '1328104065',
		'', '', '', 'Ben Harris <bjh21@cam.ac.uk>', '30x', '' ],
	      [ 'sig', '', '', '1', 'E325E4E49190393C', '1326391742',
	        '', '', '',
	        'tag:wraith.csi.cam.ac.uk,2012-01-10:starling:branch:testbox',
	        '13x', '' ],
	      [ 'sig', '', '', '1', '30B169145FAEAAC0', '1326392342',
		'', '', '', 'Ben Harris <bjh21@cam.ac.uk>', '10x', '' ],
	      ],
	},
	{ _ => [ 'uid', '-', '', '', '', '1321625216', '',
		 '99E0C029A367A7D158C039350B2A0771D24AA7AE', '',
		 'testbox.csi.cam.ac.uk', '' ],
	  sig => [
	      [ 'rev', '', '', '1', '30B169145FAEAAC0', '1328104067',
	        '', '', '', 'Ben Harris <bjh21@cam.ac.uk>', '30x', '' ],
	      [ 'sig', '', '', '1', 'E325E4E49190393C', '1321625216',
	        '', '', '',
	        'tag:wraith.csi.cam.ac.uk,2012-01-10:starling:branch:testbox',
		'13x', '' ],
	      [ 'sig', '', '', '1', '30B169145FAEAAC0', '1326392350',
		'', '', '', 'Ben Harris <bjh21@cam.ac.uk>', '10x', '' ],
	      ]
	}
	],
    sub => [
	{ _ => [ 'sub', '-', '2048', '1', '243B41440626B2CD', '1321625216',
		 '', '', '', '', '', 'e', '' ],
	  sig => [
	      [ 'sig', '', '', '1', 'E325E4E49190393C', '1321625216',
	        '', '', '',
		'tag:wraith.csi.cam.ac.uk,2012-01-10:starling:branch:testbox',
		'18x', '' ],
	      ],
	  fpr => undef,
	},
	],
  }
]);
