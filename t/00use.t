use Test::More tests => 2;

BEGIN { use_ok( 'Ucam::Starling' ) }
BEGIN { use_ok( 'Ucam::Starling::Git' ) }
