#! /usr/bin/perl

use warnings;
use strict;

=head1 NAME

starling-deploy - deploy a configuration fetched by starling-fetch

=cut

our $VERSION='0.14';

use Getopt::Long 2.33 qw/:config posix_default bundling/;
use Pod::Usage;

=head1 SYNOPSIS

 starling-deploy [--verbose]

=cut

my $verbose = 0;
my $revert = 0;
GetOptions( 'verbose|v' => \$verbose ) or pod2usage(2);

=head1 DESCRIPTION

Once a set of configuration files has been fetched with starling-fetch,
B<starling-deploy> can be used to install it into the running system.

=cut

# use YAML;

my $prefix = $ENV{PREFIX} || '';
my $starlingetc = $ENV{STARLINGETC} || "$prefix/etc/starling";
my $starlingvar = $ENV{STARLINGVAR} || "$prefix/var/lib/starling";
my $rootdir = $prefix || '/';

# my $config = YAML::LoadFile "$starlingetc/starling.conf";
# my %config = %$config;

chdir $rootdir or die "$rootdir: $!\n";

system qw(git rev-parse starling-target)
    and die "No valid configuration available.  " .
      "Run 'starling-fetch' to try to get one.\n";

exec qw(git deploy --clobber-untracked --switch),
    $verbose ? '--verbose' : (),
    'starling-target';
