package Ucam::Starling;

use warnings;
use strict;

use base qw( Exporter );
BEGIN { our @EXPORT_OK = qw( branch_to_uid ) }

=head1 NAME

 Ucam::Starling - Utility routines for Starling

=cut

use Carp;
use Error qw/:try/;
use YAML;

=head1 DESCRIPTION

=head2 C<prefix>

Returns the prefix for paths used by Starling.  This can be set to allow
testing of Starling without damaging a running system.

=cut

sub prefix () {
    return $ENV{PREFIX} || '';
}

=head2 C<etcdir>

Returns the location of Starling's I<etc> directory, usually somewhere
under C</etc>.

=cut

sub etcdir () {
    return $ENV{STARLINGETC} || prefix . "/etc/starling";
}

=head2 C<vardir>

Returns the location of Starling's I<var> directory, usually somewhere
under C</var/lib>.

=cut

sub vardir () {
    return $ENV{STARLINGVAR} || prefix . "/var/lib/starling";
}

=head2 C<rootdir>

Returns the notional root directory to be used by Starling.  This differs
from C<prefix> in that it's never empty.

=cut

sub rootdir {
    return prefix || '/';
}

=head2 C<config>

This returns the configuration most recently fetched by C<sysconfig> or
C<userconfig>.  If called before either, it currently dies, but might one
day make an intelligent guess at which to call.

=cut

my $config;
sub config () {
    if (defined($config)) {
	return $config;
    }
    croak "No configuration loaded.";
}

sub config_load ($) {
    my ($file) = @_;
    my $conf = YAML::LoadFile($file);
    ref($conf) eq 'HASH' or die "$file: top level is not a mapping\n";
    return $conf;
}

=head2 C<sysconfig>

This loads the main Starling configuration file
(C</etc/starling/starling.conf>) and returns a reference to a data
structure representing it (as deserialised from YAML).

=cut

my $configmode;
sub sysconfig () {
    $config = config_load(etcdir . "/starling.conf");
    $configmode = 'sys';
    return $config;
}


=head2 C<userconfig>

This returns the user's configuration, from C</etc/starling/user.conf>
and/or C<~/.starling>.

=cut

sub userconfig () {
    my ($dotstarling, $userconf, $sysconf);
    -f "$ENV{HOME}/.starling" and
	$dotstarling = YAML::LoadFile("$ENV{HOME}/.starling");
    -f etcdir . "/user.conf" and
	$userconf = YAML::LoadFile(etcdir . "/user.conf");
    -f etcdir . "/starling.conf" and
	$sysconf = YAML::LoadFile(etcdir . "/starling.conf");
    $dotstarling ||= { };
    $userconf ||= { };
    $sysconf ||= { };
    # Reconcile configurations by simply overriding keys in sysconf.
    $sysconf->{$_} = $userconf->{$_} for (keys %$userconf);
    $sysconf->{$_} = $dotstarling->{$_} for (keys %$dotstarling);
    $configmode = 'user';
    return ($config = $sysconf);
}

=head2 C<branch_to_uid(branch)>

This converts the name of a branch (usually a hostname) into the OpenPGP
User ID that should be used to encrypt secret files placed on that branch.
The User ID might be useful for other purposes too.

=cut

# Monkeysphere uses things that look like URIs here.  We'll follow
# suit for now.  We expect users to configure their own URI space here
# so that their branch names don't have to be globally unique (or to
# mention .csi.cam.ac.uk).
sub branch_to_uid ($) {
    my($x) = @_;
    $x =~ s/([^A-Za-z0-9-._~])/sprintf("%%%02x", ord($1))/ge;
    return config->{uidprefix} . "$x";
}

=head2 C<branches($git)>

Returns a structure representing the branches in the current repository
(actually in C<HEAD:/etc/starling/branches.conf>).  Since it extracts
the configuration from Git, it should be passed a Git object to work with.

=cut

sub branches ($) {
    my ($git) = @_;
    my $branches;
    try {
	my $branches_conf =
	    $git->command(qw(cat-file blob HEAD:etc/starling/branches.conf));
	$branches = YAML::Load($branches_conf);
    } otherwise {
	$branches = { };
    };

    # The extant branches should be (a) anything that's a key in
    # branches.conf, plus (b) anything listed as the child of another
    # branch.

    # Flesh out the branch structure.
    for my $parent (keys %$branches) {
	$branches->{$parent}->{children} ||= [ ];
	$branches->{$parent}->{parents} ||= [ ];
	for my $child (@{$branches->{$parent}->{children}}) {
	    $branches->{$child} ||= { children => [ ] };
	    push @{$branches->{$child}->{parents}}, $parent;
	}
    }
    return $branches;
}

=head2 C<gpg>

This returns the command to use to run GnuPG.  The returned value will be
an array that can be passed as the initial arguments to C<system> or
similar, namely a command name and some options.

=cut

sub gpg () {
    my @gpg = ('gpg');
    push @gpg, ('--keyserver', config->{keyserver})
	if exists config->{keyserver};
    if ($configmode eq 'sys') {
	my $gpghome =  vardir . "/gnupg";
	push @gpg, ('--homedir', $gpghome);
	# XXX does this really belong here?
	-d $gpghome or mkdir $gpghome, 0700 or die "mkdir $gpghome: $!\n";
    }
    return @gpg;
}

sub run_gpg (@) {
    if (system('gpg', @_)) {
	$? == -1 and die "gpg: $!\n";
	die "gpg failed\n";
    }
}

=head2 C<gpg_keylist(@arg)>

This runs GnuPG asking for a listing of its keyring and parses the result
into a vaguely sensible data structure.  C<@args> is a set of additional
arguments to pass to C<gpg>.

=cut

sub gpg_keylist (@) {
    open my $gpg, '-|', gpg, '--fingerprint', '--with-colons',
        '--fixed-list-mode', @_ or die "gpg: $!\n";
    my @line = <$gpg>;
    close $gpg or die "gpg: $!\n";
    return gpg_parse_keylist(@line);
}


sub gpg_parse_keylist (@) {
    # This format is cleaner to parse backwards.
    my($sig, $fpr, $sub, $uid, $key) = ([], undef, [], [], []);
    for (reverse @_) {
	chomp;
	my @field = map gpg_unescape($_), split(/:/, $_, -1);
	my $type = $field[0];
	if ($type eq 'fpr') {
	    $fpr = \@field;
	}
	if ($type eq 'sig' || $type eq 'rev') {
	    unshift @$sig, \@field;
	}
	if ($type eq 'sub' || $key eq 'ssb') {
	    unshift @$sub, { fpr => $fpr, sig => $sig, _ => \@field };
	    undef $fpr; $sig = [];
	}
	if ($type eq 'uid') {
	    unshift @$uid, { sig => $sig, _ => \@field };
	    $sig = [];
	}
	if ($type eq 'pub' || $key eq 'sec') {
	    unshift @$key,
	        { fpr => $fpr, uid => $uid, sub => $sub, _ => \@field };
	    $uid = []; $sub = [];
	}
    }
    return @{$key};
}

sub gpg_unescape ($) {
    my($x) = @_;
    $x =~ s/\\x(..)/chr hex $1/eg;
    return $x;
}

1;
