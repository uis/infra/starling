=head1 NAME

Ucam::Starling::Git - Git module with fewer annoyances

=head1 SYNOPSIS

 use Ucam::Starling::Git

=cut

package Ucam::Starling::Git;

use base qw/Git/;

=head1 DESCRIPTION

C<Ucam::Starling::Git> is a wrapper around the C<Git> module that comes
with Git.  It provides better support for having the working tree
separate from the repository and a more functional command_close_bidi_pipe.

=cut

use Cwd qw(abs_path getcwd);

sub repository {
    my $self = shift;
    @_ == 1 and @_ = (Directory => @_);
    my %useropt = @_;

    # Git::repository makes a complete hash of things when we have the
    # working tree and repository separated.  For instance, it goes
    # around reporting that the working tree is in the repository.
    # Here, we fix up the simple case where the user hasn't specified
    # any locations, so that at least that works.

    my %gitopt = ();
    unless (exists $useropt{Directory} or exists $useropt{WorkingCopy} or
	    exists $useropt{WorkingCopy} or exists $useropt{Repository}) {
	%gitopt = ( Repository =>
		    abs_path(Git::command_oneline qw(rev-parse --git-dir)) );

	if ((Git::command_oneline qw(rev-parse --is-inside-work-tree)) eq
	    'true') {
	    my $wsd = Git::command_oneline qw(rev-parse --show-prefix);
	    if (defined $wsd) {
		$gitopt{WorkingSubdir} = $wsd;
	    }
	}

	if ((Git::command_oneline qw(rev-parse --is-bare-repository)) eq
	    'false') {
	    my $wc = 
		Git::command_oneline qw(rev-parse --show-toplevel);
	    if ($wc ne '--show-toplevel') {
		$gitopt{WorkingCopy} = $wc;
	    } elsif (exists $ENV{GIT_WORK_TREE}) {
		$gitopt{WorkingCopy} = $ENV{GIT_WORK_TREE};
	    } elsif (defined($wc = Git::config('core.worktree'))) {
		$gitopt{WorkingCopy} = $wc;
	    } elsif (exists $gitopt{WorkingSubdir}) {
		my $wsd = getcwd();
		if ($wsd =~ s,\Q/$gitopt{WorkingSubdir}$,,) {
		    $wsd == '' and $cwd = '/';
		    $gitopt{WorkingCopy} = $wsd;
		}
	    }

	}
    }
    return $self->SUPER::repository(%useropt, %gitopt);
}

# Git::command_close_bidi_pipe (at least in Git.pm 0.01) is
# broken in at least two ways: it refuses to take a repository
# as its first argument, and it gets confused by being passed
# a closed or undef filehandle.

sub command_close_bidi_pipe {
    my ($self, $pid, $from, $to, $ctx) = Git::_maybe_self(@_);

    for ($from, $to) {
	next if defined $_;
	open $_, "</dev/null" or die "/dev/null: $!";
    }
    return Git::command_close_bidi_pipe($pid, $from, $to, $ctx);
}

# This is rude, but some versions of Git.pm break subclassing without.

sub Git::_maybe_self { UNIVERSAL::isa($_[0], 'Git') ? @_ : (undef, @_); }

=head2 C<unquote_c_style>

Returns its argument with Git's standard quoting undone.

=cut

my %unquote = (
    'a' => "\a", 'b' => "\b", 'f' => "\f", 'n' => "\n",
    'r' => "\r", 't' => "\t", 'v' => "\x0b", "\\" => "\\", '"' => '"' );

sub unquote_c_style {
    my ($in) = @_;
    return $in unless $in =~ s/^"//;
    $in =~ s/(?:\\([abfnrtv"\\])|\\([0-3][0-7][0-7])|".*$)/
             defined $1 ? $unquote{$1} :
	     defined $2 ? chr(oct($2)) :
             ""
            /eg;
    return $in;
}


1;

