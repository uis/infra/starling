#! /usr/bin/python
"""

=head1 NAME

starling-decrypt - Decrypt secret configuration files

=head1 SYNOPSIS

 starling-decrypt [--deploy] [--verbose] [<old-ref> <new-ref>]

=head1 DESCRIPTION

B<starling-decrypt> expects to be run as part of the post-deploy hook
of C<git deploy>.  It looks for files with names ending C<.gpg> that
have changed and attempts to decrypt them using C<gpg>.

If run with no arguments then it will treat all files in the current
Git repository as having changed and decrypt all of them.

If the C<--deploy> option is supplied, B<starling-decrypt> adapts its
behaviour to be suitable for deploying into a live system.  In particular,
it uses Starling's own GnuPG keyring and sets the permissions on decrypted
files to match their originals (which are assumed to have been set by
C<git deploy>).

=cut

"""

import argparse
import os
import sys

import re

import os.path
import tempfile
import subprocess
import errno

def main():
    parser = argparse.ArgumentParser(description="Decrypt secret configuration files")

    parser.add_argument('--deploy', action='store_true')
    parser.add_argument('--verbose', '-v', action='store_true')
    parser.add_argument('old', nargs='?')
    parser.add_argument('new', nargs='?')

    cfg = parser.parse_args()

    if (cfg.old, cfg.new) == (None, None):
        # If we have no arguments, pretend all files in HEAD have just been
        # created.  4b825dc is the empty tree.
        cfg.old = '4b825dc642cb6eb9a060e54bf8d69288fbee4904'
        cfg.new = 'HEAD'

    if cfg.old == None or cfg.new == None:
        parser.error("old and new must be provided together or not at all.")


    prefix = os.environ.get('PREFIX', '/')
    starlingetc = os.environ.get('STARLINGETC',
                                 os.path.join(prefix, "etc/starling"))
    starlingvar = os.environ.get('STARLINGVAR',
                                 os.path.join(prefix,"var/lib/starling"))
    rootdir = prefix;

    if cfg.deploy:
        gpghome = os.path.join(starlingvar, "gnupg")
        gpgopts = ["--homedir", gpghome]
    else:
        gpgopts = []

    os.chdir(git_commandline(['rev-parse', '--show-cdup']))

    files = git_commandz(['diff-tree', '-z', '-r', '--name-only',
                          '--diff-filter=AMT', '--no-renames',
                          cfg.old, cfg.new, '--'])
    attrs = git_check_attr(['starling.decrypt'], files)
    encryptedfiles = filter(lambda x: attrs[x]['starling.decrypt'], files)

    for f in encryptedfiles:
        if not f.endswith('.gpg'):
            exit("encrypted file with surprising name: %s" % (f))
        plain = f[:-4]
        if cfg.verbose: print('decrypting %s to %s' % (f, plain))
        tmp = tempfile.NamedTemporaryFile(prefix=".#",
                                          dir=os.path.dirname(plain))
        subprocess.check_call(['gpg'] + gpgopts + ['--decrypt', f], stdout=tmp)
        if cfg.deploy:
            st = os.stat(f)
            os.fchown(tmp.fileno(), st.st_uid, st.st_gid)
            os.fchmod(tmp.fileno(), st.st_mode)
        os.rename(tmp.name, plain)
        tmp.delete = False


def git_command(cmd):
    "Run git and return its output, raising an exception on failure."
    cmd = ['git'] + cmd
    if hasattr(subprocess, 'check_output'):
        out = subprocess.check_output(cmd)
    else:
        child = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        out = child.communicate()[0]
        if child.returncode:
            raise subprocess.CalledProcessError(child.returncode, cmd)
    return out

def git_commandz(cmd):
    "Run a git command and split the output on NULs."
    return git_command(cmd).split('\0')[:-1]

def git_commandlf(cmd):
    "Run a git command and split the output on LFs."
    return git_command(cmd).split('\n')[:-1]

def git_commandline(cmd):
    "Run a git command that returns a single LF-terminated string."
    return git_command(cmd)[:-1]

git_unquote_map = { 'a': "\a", 'b': "\b", 'f': "\f", 'n': "\n",
                    'r': "\r", 't': "\t", 'v': "\v", '\\': "\\", '"': '"' }
def git_unquote_c_style(s):
    def unquote_sym(m):
        s = m.group(1)
        if re.match(r'[0-3][0-7][0-7]$', s):
            return chr(oct(s))
        return git_unquote_map[s]
    if s.startswith('"') and s.endswith('"'):
        return re.sub(r'\\([abfnrtv"\\]|[0-3][0-7][0-7])', unquote_sym, s[1:-1])
    else:
        return s

def git_check_attr(attrs, files):
    """
    Given a list of attributes and an iterator of filenames, get the attributes
    of that file.  Returns a dictionary of dictionaries.
    """
    result = {}
    # XXX should split files to stay within ARG_MAX
    for l in git_commandlf(['check-attr'] + attrs + ['--'] + files):
        file, attr, txtval = l.rsplit(': ', 2)
        file = git_unquote_c_style(file)
        if txtval == 'unspecified':
            val = None
        elif txtval == 'set':
            val = True
        elif txtval == 'unset':
            val = False
        else:
            val = txtval
        result.setdefault(file, {})
        result[file][attr] = val
    return result

try:
    main()
except subprocess.CalledProcessError as e:
    # The called process should already have printed an error message.
    exit(e.returncode)
except Exception as e:
    exit(e)
except KeyboardInterrupt:
    exit(129)
