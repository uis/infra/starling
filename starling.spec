Name: starling
Summary: Git-based configuration management system
# If updating version number here, also update Makefile.PL and
# debian/changelog.
Version: 0.18
Release: 1
License: GPLv2
Group: Local
Source: %{name}-%{version}.tar.gz
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-root
Requires: perl(YAML) perl(Git) gpg python-base >= 2.6

%Description
This package provides parts of the Starling configuration management system,
a crude system built on top of Git for handling configuration files in an
environment without a fully-trusted configuration server.

%Prep
%setup

%Build
perl Makefile.PL
make

%Install
rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=${RPM_BUILD_ROOT} install_vendor
%perl_process_packlist

%Files
%defattr(-,root,root)
%doc README
/usr/bin/starling-fetch
/usr/bin/starling-deploy
/usr/bin/starling-encrypt
/usr/bin/starling-initbox
/usr/bin/starling-inituser
/usr/bin/starling-post-deploy
/usr/bin/starling-passwd
/usr/bin/starling-pull
/usr/bin/starling-push
/usr/bin/starling-newbox
/usr/bin/starling-decrypt
/usr/bin/starling-status
/usr/bin/starling-commit
/usr/share/man/man1/starling-fetch.1.gz
/usr/share/man/man1/starling-deploy.1.gz
/usr/share/man/man1/starling-encrypt.1.gz
/usr/share/man/man1/starling-initbox.1.gz
/usr/share/man/man1/starling-inituser.1.gz
/usr/share/man/man1/starling-passwd.1.gz
/usr/share/man/man1/starling-post-deploy.1.gz
/usr/share/man/man1/starling-pull.1.gz
/usr/share/man/man1/starling-push.1.gz
/usr/share/man/man1/starling-newbox.1.gz
/usr/share/man/man1/starling-decrypt.1.gz
/usr/share/man/man1/starling-status.1.gz
/usr/share/man/man1/starling-commit.1.gz
/usr/share/man/man3/Ucam::Starling.3pm.gz
/usr/share/man/man3/Ucam::Starling::Git.3pm.gz
/usr/share/man/man3/starling.3pm.gz
/usr/share/man/man3/starling.conf.3pm.gz
%{perl_vendorarch}/auto/%{name}/.packlist
%{perl_vendorlib}/Ucam/Starling.pm
%{perl_vendorlib}/Ucam/Starling/Git.pm
%{perl_vendorlib}/starling.conf.pod
%{perl_vendorlib}/starling.pod
/var/adm/perl-modules/%{name}
